# CV

## Descripción

SPA Para poner en práctica conocimiento de front-end y versionado. Realizado con HTML, CSS, JS, Mustache. Bajo el concepto de mobile first.

# Roadmap

1. El primer paso es la creación del template en HTML y CSS.
2. Agregar media queries para hacer el sitio responsive
3. Una vez finalizada la etapa de maquetación, se procederá a incluir la animación del título y el uso de imágenes.
4. Se Agregará modo "dark" para manipulación del DOM con JS.
5. Integrar el uso de motor de plantillas (mustache)
6. Deploy

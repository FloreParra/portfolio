const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: "/src/index.js",
  plugins: [
    new HtmlWebpackPlugin({
      template: "src/template.html",
    }),
  ],
  module: {
    rules: [
      { test: /\.htmal$/, use: ["html-loader"] },
      {
        test: /\.(svg|png|git|jpg)$/,
        use: {
          loader: "file-loader",
          options: {
            name: "[name].[hash].[ext]",
            output: "imgs",
          },
        },
      },
    ],
  },
};
